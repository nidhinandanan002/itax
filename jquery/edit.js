$(document).ready(function() {
    $('#password').keydown(function() {
        check_password();

    });

    $('#cpassword').keydown(function() {
        check_password_match();

    });

    //function for password validation  
    function check_password() {
        var pass_length = $('#password').val().length;
        if (pass_length < 5) {
            $('#password_msg').addClass('error_msg');
            $('#password_msg').html('Atleast 5 characters needed!');
            $('#password_msg').show();
            err_pswd = true;

        } else {
            $('#password_msg').hide();
        }
    }

    function check_password_match() {
        var pswd = $('#password').val();
        var re_pswd = $('#cpassword').val();
        if (re_pswd != pswd) {
            $('#cpassword_msg').addClass('error_msg');
            $('#cpassword_msg').html('Passwords do not match!');
            $('#cpassword_msg').show();
            err_pswd_match = true;

        } else {
            $('#cpassword_msg').hide();
        }
    }
});
