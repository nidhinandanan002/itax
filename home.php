<?php

include("dbconn.php");

$name = $_SESSION['name'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Akaya+Telivigala&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css\home_style.css">
    <title>Document</title>
</head>
<header class="topnav">
    <div>
        <a href="logout.php">LOGOUT</a>
    </div>
    <div class="icon">
        <?php
        $icon = substr($name, 0, 1);
        echo $icon;
        ?>

    </div>

</header>

<body>
    <div class="wrapper">
        <div class="sidebar">
            <ul class="list">
                <li><a href="calculate.php">CALCULATE TAX</a></li>
                <li><a href="edit_details.php">EDIT PROFILE</a></li>
            </ul>
        </div>
    </div>

</body>

</html>