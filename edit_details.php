<?php
session_start();
include('dbconn.php');
include('home.php');

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Document</title>
    <link rel="stylesheet" href="css/edit_style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="jquery/edit.js"></script>
</head>

<body>

    <div>
        <h1>EDIT</h1>
        <div id="message"></div>
    </div>
x
    <div class="container">
        <form class="edit-form" action="edit_details.php" method="POST">

            <label for="name">FULL NAME</label>
            <input type="text" name="name" value="<?php echo $_SESSION['name']; ?>" id="">

            <label for="password">Password</label>
            <span id="password_msg"></span>
            <input type="password" name="password" id="password">

            <label for="cpassword">Confirm Password</label>
            <span id="cpassword_msg"></span>
            <input type="password" name="cpassword" id="cpassword">

            <button type="submit" name="submit" class="btn">UPDATE</button>
            <!-- <p id="form-message">para</p> -->
        </form>
    </div>
    <?php
    if (isset($_POST['submit'])) {

        $password = $_POST['password'];
        $cpassword = $_POST['cpassword'];
        //$this_password=md5($_POST['password']);
        $newpassword = md5($password);
        $id = $_SESSION['id'];

        $errorEmpty = false;
        $errorMismatch = false;

        if (empty($password) || empty($cpassword)) {
            echo    "<center class='error_msg'>Fill in all fields!</center>";
            $errorEmpty = true;
        } else {
            if ($password != $cpassword) {
                echo "<center class='error_msg'>Passwords do not match!</center>";
               $errorMismatch = true;
                //header('location:edit.php');
            } else if ($password == $cpassword) {
                $sql = "update users set password='".$newpassword."' where user_id='".$id."' ";
                $result = mysqli_query($conn, $sql);
                if($result==true)
		{
			echo "<center>Password updated successfully!</center>";
		}
            }
        }
    }
    ?>
</body>

</html>
