<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Akaya+Telivigala&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css\login_style.css">
    <title>login page</title>

</head>

<body>
    <!-- <img class="bg" src="images\\bg1.jpg" alt="background image" > -->
    <div class="container">
        <form class="form-box" action="auth.php" method="POST">
            <div class="email">
                <label for="email">Email</label>
                <input type="text" name="emailid" id="emailid" placeholder="Enter your email">
            </div>
            <div class="pswd">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" placeholder="Enter Your Password">
            </div>
            <div class="submitbtn">
                <button type="submit" name="submit" value="Login" id=btn>Login</button><br><br>
            </div>
        </form>



        <?php
        if (isset($_SESSION['error'])) {
            $error = $_SESSION['error'];
            echo "<span>$error</span>";
        }

        ?>
</body>

</html>