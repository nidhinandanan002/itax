<?php
session_start();
// echo $_SESSION['name'];
// echo $_SESSION['id'];
include('dbconn.php');
include('home.php');

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Akaya+Telivigala&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/calc_style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="jquery/tax.js"></script>
    <title>Tax calculate</title>
</head>

<body>
    <div>
        <h1>CALCULATE TAX</h1>
        <div id="message"></div>
    </div>
    <div class="container">
        <form class="calc-form" action="" method="post">

            <label for="name">Full Name</label>
            <input type="text" name="name" value="<?php echo $_SESSION['name']; ?>">
            <label for="income">Annual Income</label>
            <span id="income_msg"></span>
            <input type="text" name="income" id="income" required>
            <label for="age">Age</label>
            <span id="age_msg"></span>
            <input id="age" type="number" name="age" required>
            <button type="submit" name="calculate" class="btn">CALCULATE</button>

        </form>
    </div>
    <?php
    if (isset($_POST['calculate'])) {
        $income = $_POST['income'];
        $age = $_POST['age'];
        $data = ("SELECT * FROM admin_income WHERE age<$age and age in (SELECT max(age) FROM admin_income WHERE age<$age order by age desc) order by age");
        //print_r($data);
        $result = mysqli_query($conn, $data);
        //print_r($result);
        $count = mysqli_num_rows($result);
        global $totaltax;
        $i = 0;
        $tax = 0;
        $taxable_amount = $income;
        while ($row = mysqli_fetch_assoc($result)) {
            $i++;
            //$start_income =$row['start_incom'];
            //$end_income =$row['end_incom'];
            //$age =$row['age'];
            //echo("<br> $i .age : $age , start_income : $start_income, end income : $end_income");

            if ($income < $row['start_incom']) {
                echo "<b>TAX NOT APPLICABLE!</b>";
                $totaltax = $tax;
                break;
            } else{

                if ($income > $row['end_incom']) {
                    // echo "income:".$income."   "."end-income: ".$row['end_incom'];
                    $tax = $row['percentage'] * ($row['end_incom'] - $row['start_incom']) / 100;
                    $totaltax += $tax;
                    //echo "<br>TAX APPLIED IN SLAB " . $i . ": <b>" . $tax . "</b>";
                    //echo "<br>TOTAL TAX AT THE END OF SLAB " . $i . ": <b>" . $totaltax . "</b><br>";
                } elseif ($income <= $row['end_incom']){

                    //echo "in else";
                    $tax = ($income - $row['start_incom']) * $row['percentage'] / 100;
                    $totaltax += $tax;
                    //echo "<br>TAX APPLIED IN SLAB " . $i . ": <b>" . $tax . "</b>";
                    //echo "<br>TOTAL TAX AT THE END OF SLAB " . $i . ": <b>" . $totaltax . "</b><br>";
                    break;
                }
            }
        }
        echo "<center><b>YOU HAVE TO PAY TAX AMOUNT OF : $totaltax</center>";
    }
    ?>
</body>

</html>
